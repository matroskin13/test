function advertCtrl($stateParams, CarsFactory) {
	this.data = {
		advert: false
	}
	CarsFactory.getCar($stateParams.advertId, this.data);
}
function advertsCtrl(CarsFactory) {
	this.limitStep = 2;
	this.limit = 2;
	this.data = {
		cars: false,
		brands: [
			{brandId: "", brandTitle: "Все марки"},
			{brandId: 1, brandTitle: "Honda"},
			{brandId: 2, brandTitle: "BMW"}
		],
		models: [
			{modelId: "", brandId: "", modelTitle: "Все модели"},
			{modelId: 1, brandId: 2, modelTitle: "X1"},
			{modelId: 2, brandId: 2, modelTitle: "X5"},
			{modelId: 3, brandId: 1, modelTitle: "Civic"},
			{modelId: 4, brandId: 1, modelTitle: "Accord"}
		]
	}
	this.query = "";
	this.searchSettings = {
		brandId: "",
		modelId: ""
	}
	this.searchSettingsForYear = {
		value: 'year',
		min: "",
		max: ""
	}
	this.searchSettingsForPrice = {
		value: 'price',
		min: "",
		max: ""
	}
	this.getMore = function() {
		this.limit += this.limitStep;
	}
	CarsFactory.getCars(this.data);
}
function homeCtrl(CarsFactory) {
	this.data = {
		cars: false
	}
	CarsFactory.getCars(this.data);
}
function imageDirective() {
	var imageDirective = {};
	imageDirective.template = '<div class="load" ng-if="load"></div>';
	imageDirective.scope = {
		src: "=src"
	};
	imageDirective.link = function(scope, element, attrs) {
		var imagePath = scope.src,
			image = new Image();
		scope.load = true;
		image.onload = function() {
			element[0].appendChild(image);
			scope.load = false;
			scope.$apply();
		}
		image.src = imagePath;
	}
	return imageDirective;
}
function compareFilter() {
	return function(items, compareSettings) {
		var updateList = [], value = compareSettings.value, isError, item;
		for (var i = 0; i < items.length; i++) {
			isError = false;
			item = items[i];
			if (!item[value]) {
				isError = true;
			}
			if (compareSettings.min && compareSettings.min != "" && parseInt(compareSettings.min) > item[value]) {
				isError = true;
			}
			if (compareSettings.max && compareSettings.max != "" && parseInt(compareSettings.max) < item[value]) {
				isError = true;
			}
			if (!isError) {
				updateList.push(item);
			} 
		}
		return updateList; 
	}
}
function CarsFactory(ServerFactory) {
	var CarsFactory = {};
	/*
		Получение списка машин. Входные параметры:
		settings - объект с критериями поиска
			settings.fromPrice - начиная от цены(по умолчанию 0)
			settings.toPrice - до цены(по умолчанию null)
			settings.brandId - марка машины
			settings.modelId - модель машины
			settings.year - год выпуска машины
			settings.sort - вариант сортировки (1 - последние, 2 - популярные)
		scopeObjectFromController - объект со переменными скопа из контроллера
	*/
	CarsFactory.getCars = function(scopeObjectFromController) {
		var requestParams = {
			url: "json/adverts.json",
			method: "get"
		}
		ServerFactory.request(requestParams.url, requestParams.method, {}).then(function(response){
			scopeObjectFromController.cars = response;
		});
	}
	/*
		Получение конкретной машины. Входные параметры:
		carId - id объявления
	*/
	CarsFactory.getCar = function(carId, scopeObjectFromController) {
		var requestParams = {
			url: "json/adverts.json",
			method: "get"
		}
		ServerFactory.request(requestParams.url, requestParams.method, {}).then(function(response){
			for (var i in response) {
				if (response[i].advertId == carId) {
					scopeObjectFromController.advert = response[i];
				}
			}
		});
	}
	/*
		Получение марок машин.
	*/
	CarsFactory.getBrands = function() {
		var requestParams = {
			url: "/get/brands",
			method: "get"
		}
	}
	/*
		Получение моделей машин.
	*/
	CarsFactory.getModels = function() {
		var requestParams = {
			url: "/get/models",
			method: "get"
		}
	}
	return CarsFactory;
}
function ServerFactory($http, $q) {
	var ServerFactory = {};
	ServerFactory.request = function(url, method, params) {
		var defer = $q.defer(),
			promise = defer.promise,
			resolveSettins;
		$http[method](url, params)
			.success(function(data, status){
				resolveSettins = {
					response: data,
					status: status
				}
				defer.resolve(data);
			})
			.error(function(data){
				console.log(data);
			})
		return promise;
	};
	return ServerFactory;
}
angular.module('app', ['ui.router'])
	.config(config)
	.controller('homeCtrl', homeCtrl)
	.controller('advertsCtrl', advertsCtrl)
	.factory('CarsFactory', CarsFactory)
	.factory('ServerFactory', ServerFactory)
	.filter('compare', compareFilter)
	.directive('myImage', imageDirective);

function config($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('home', {
			url: '/', 
			controller: homeCtrl,
			controllerAs: "home",
			templateUrl: "templates/home.html"
		})
		.state('adverts', {
			url: "/adverts",
			controller: advertsCtrl,
			controllerAs: "adverts",
			templateUrl: "templates/adverts.html"
		})
		.state('advert', {
			url: '/advert/:advertId',
			controller: advertCtrl,
			controllerAs: "advert",
			templateUrl: "templates/advert.html"
		});
}