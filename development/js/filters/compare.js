function compareFilter() {
	return function(items, compareSettings) {
		var updateList = [], value = compareSettings.value, isError, item;
		for (var i = 0; i < items.length; i++) {
			isError = false;
			item = items[i];
			if (!item[value]) {
				isError = true;
			}
			if (compareSettings.min && compareSettings.min != "" && parseInt(compareSettings.min) > item[value]) {
				isError = true;
			}
			if (compareSettings.max && compareSettings.max != "" && parseInt(compareSettings.max) < item[value]) {
				isError = true;
			}
			if (!isError) {
				updateList.push(item);
			} 
		}
		return updateList; 
	}
}