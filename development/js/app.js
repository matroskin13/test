angular.module('app', ['ui.router'])
	.config(config)
	.controller('homeCtrl', homeCtrl)
	.controller('advertsCtrl', advertsCtrl)
	.factory('CarsFactory', CarsFactory)
	.factory('ServerFactory', ServerFactory)
	.filter('compare', compareFilter)
	.directive('myImage', imageDirective);

function config($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('home', {
			url: '/', 
			controller: homeCtrl,
			controllerAs: "home",
			templateUrl: "templates/home.html"
		})
		.state('adverts', {
			url: "/adverts",
			controller: advertsCtrl,
			controllerAs: "adverts",
			templateUrl: "templates/adverts.html"
		})
		.state('advert', {
			url: '/advert/:advertId',
			controller: advertCtrl,
			controllerAs: "advert",
			templateUrl: "templates/advert.html"
		});
}