function advertsCtrl(CarsFactory) {
	this.limitStep = 2;
	this.limit = 2;
	this.data = {
		cars: false,
		brands: [
			{brandId: "", brandTitle: "Все марки"},
			{brandId: 1, brandTitle: "Honda"},
			{brandId: 2, brandTitle: "BMW"}
		],
		models: [
			{modelId: "", brandId: "", modelTitle: "Все модели"},
			{modelId: 1, brandId: 2, modelTitle: "X1"},
			{modelId: 2, brandId: 2, modelTitle: "X5"},
			{modelId: 3, brandId: 1, modelTitle: "Civic"},
			{modelId: 4, brandId: 1, modelTitle: "Accord"}
		]
	}
	this.query = "";
	this.searchSettings = {
		brandId: "",
		modelId: ""
	}
	this.searchSettingsForYear = {
		value: 'year',
		min: "",
		max: ""
	}
	this.searchSettingsForPrice = {
		value: 'price',
		min: "",
		max: ""
	}
	this.getMore = function() {
		this.limit += this.limitStep;
	}
	CarsFactory.getCars(this.data);
}