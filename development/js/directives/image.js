function imageDirective() {
	var imageDirective = {};
	imageDirective.template = '<div class="load" ng-if="load"></div>';
	imageDirective.scope = {
		src: "=src"
	};
	imageDirective.link = function(scope, element, attrs) {
		var imagePath = scope.src,
			image = new Image();
		scope.load = true;
		image.onload = function() {
			element[0].appendChild(image);
			scope.load = false;
			scope.$apply();
		}
		image.src = imagePath;
	}
	return imageDirective;
}