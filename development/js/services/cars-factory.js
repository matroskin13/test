function CarsFactory(ServerFactory) {
	var CarsFactory = {};
	/*
		Получение списка машин. Входные параметры:
		settings - объект с критериями поиска
			settings.fromPrice - начиная от цены(по умолчанию 0)
			settings.toPrice - до цены(по умолчанию null)
			settings.brandId - марка машины
			settings.modelId - модель машины
			settings.year - год выпуска машины
			settings.sort - вариант сортировки (1 - последние, 2 - популярные)
		scopeObjectFromController - объект со переменными скопа из контроллера
	*/
	CarsFactory.getCars = function(scopeObjectFromController) {
		var requestParams = {
			url: "json/adverts.json",
			method: "get"
		}
		ServerFactory.request(requestParams.url, requestParams.method, {}).then(function(response){
			scopeObjectFromController.cars = response;
		});
	}
	/*
		Получение конкретной машины. Входные параметры:
		carId - id объявления
	*/
	CarsFactory.getCar = function(carId, scopeObjectFromController) {
		var requestParams = {
			url: "json/adverts.json",
			method: "get"
		}
		ServerFactory.request(requestParams.url, requestParams.method, {}).then(function(response){
			for (var i in response) {
				if (response[i].advertId == carId) {
					scopeObjectFromController.advert = response[i];
				}
			}
		});
	}
	/*
		Получение марок машин.
	*/
	CarsFactory.getBrands = function() {
		var requestParams = {
			url: "/get/brands",
			method: "get"
		}
	}
	/*
		Получение моделей машин.
	*/
	CarsFactory.getModels = function() {
		var requestParams = {
			url: "/get/models",
			method: "get"
		}
	}
	return CarsFactory;
}