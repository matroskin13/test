function ServerFactory($http, $q) {
	var ServerFactory = {};
	ServerFactory.request = function(url, method, params) {
		var defer = $q.defer(),
			promise = defer.promise,
			resolveSettins;
		$http[method](url, params)
			.success(function(data, status){
				resolveSettins = {
					response: data,
					status: status
				}
				defer.resolve(data);
			})
			.error(function(data){
				console.log(data);
			})
		return promise;
	};
	return ServerFactory;
}