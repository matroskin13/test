// Custom plugins
var gulp = require('gulp'),
	concat = require('gulp-concat');

// Plugins for js
var uglify = require('gulp-uglify');

// Plugins for css
var minifyCSS = require('gulp-minify-css'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer');

var development = "../",
	devJs = development + "js/",
	devCss = development + "scss/";

var production = "../../",
	productionCss = production + "css/",
	productionCssFile = "style.css",
	productionJs = production + "js/",
	productionJsFile = "main.js";

var bowerComponents = "../bower_components/";

var libsJs = [
	bowerComponents + "angular/angular.js",
	bowerComponents + "ui-router/release/angular-ui-router.js"
];

var filesJs = [
	devJs + "controllers/*.js",
	devJs + "directives/*.js",
	devJs + "filters/*.js",
	devJs + "services/*.js",
	devJs + "app.js"
];

var filesCss = [
	devCss + "reset.scss",
	devCss + "settings.scss",
	devCss + "custom.scss",
	devCss + "*.scss"
];

gulp.task('css', function(){
	gulp.src(filesCss)
		.pipe(concat('style.scss'))
		.pipe(autoprefixer())
		.pipe(sass())
		.pipe(gulp.dest(productionCss));
})

gulp.task('js', function(){
	gulp.src(filesJs)
		.pipe(concat(productionJsFile))
		.pipe(gulp.dest(productionJs));
})

gulp.task('js-libs', function(){
	gulp.src(libsJs)
		.pipe(concat('libs.js'))
		.pipe(gulp.dest(productionJs));
})

gulp.task('watch', ['js', 'css'], function(){
	gulp.watch(filesCss, ['css']);
	gulp.watch(filesJs, ['js']);
})